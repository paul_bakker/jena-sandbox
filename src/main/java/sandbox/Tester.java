package sandbox;


import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.PrintUtil;

/**
 * @Author Paul Bakker - paul.bakker@luminis.eu
 * This example only demonstrates simple Jena usage. The curriculum.owl scheme is changed to include an rdfs:subtypeof for subjectcurriculum because
 * skos is not working in this example yet.
 */
public class Tester {
    public static void main(String[] args) {
        Model model = ModelFactory.createRDFSModel(ModelFactory.createDefaultModel());
        Model schema = FileManager.get().loadModel("curriculum.owl");

        Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
        reasoner = reasoner.bindSchema(schema);
        InfModel infmodel = ModelFactory.createInfModel(reasoner, model);

        infmodel.add(model.createStatement(
                model.createResource("http://example/curriculum/java"),
                model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                model.createResource("http://www.luminis.net/ontologies/arl/1.2/curriculum.owl#SubjectCurriculum")));


        //Should return all subjectcurriculum too because it's an rdfs:subTypeOf
        String queryString = ""
                + " PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>"
                + " PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                + " PREFIX curriculum:<http://www.luminis.net/ontologies/arl/1.2/curriculum.owl#>"
                + " PREFIX skos:<http://www.w3.org/2004/02/skos/core#>"
                + " SELECT ?curriculum"
                + " WHERE {"
                + " ?curriculum rdf:type curriculum:Curriculum"
                + " }";


        infmodel.write(System.out);
        Query query = QueryFactory.create(queryString);
        QueryExecution qexec = QueryExecutionFactory.create(query, infmodel);
        try {
            ResultSet results = qexec.execSelect();
            for (; results.hasNext(); ) {
                QuerySolution soln = results.nextSolution();
                RDFNode x = soln.get("curriculum");
                System.out.println("X" + x.toString());
            }
        } finally {
            qexec.close();
        }

        Resource nForce = infmodel.getResource("http://example/curriculum/java");
        System.out.println("nForce :");
        printStatements(infmodel, nForce, null, null);

    }

    public static void printStatements(Model m, Resource s, Property p, Resource o) {
        for (StmtIterator i = m.listStatements(s, p, o); i.hasNext(); ) {
            Statement stmt = i.nextStatement();
            System.out.println(" - " + PrintUtil.print(stmt));
        }
    }

}
